import { Counter } from '@2023-10-05-angular-testing/testing';
import { CounterComponent } from './counter.component';
import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

describe('CounterComponent', () => {
  let fixture: ComponentFixture<CounterComponent>;
  let component: CounterComponent;

  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CounterComponent, HttpClientTestingModule],
    });
    fixture = TestBed.createComponent(CounterComponent);
    component = fixture.componentInstance;
    http = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
    expect(component.variable).toBe(0);
    expect(component.variable).toEqual(0);
    expect(component.secVar).toBeNull();
    expect(component.counter1).toBeNull();
  });

  it('ngOnInit()', () => {
    fixture.detectChanges();
    component.ngOnInit();

    expect(component.counter1).not.toBeNull();
    expect(component.counter1 instanceof Counter).toBe(true);
    expect(component.counter1).toBeInstanceOf(Counter);
    expect(component.counter2).toBeInstanceOf(Counter);
  });

  describe('generateOutput()', () => {
    it('should not output the counters', () => {
      component.generateOutput();
      expect(component.output).toEqual({
        one: -1,
        two: -1,
      });
    });

    // it.failing('failing test', () => {
    //   component.generateOutput();
    //   expect(component.output).toEqual({
    //     one: 0,
    //     two: 0,
    //   });
    // });

    it('should output the counters', () => {
      // arrange
      fixture.detectChanges();
      // act
      component.generateOutput();
      // assert
      expect(component.output).toEqual({
        one: 0,
        two: 0,
      });
    });
  });

  it('should display the output after change detection', () => {
    fixture.detectChanges();
    const element: HTMLElement = fixture.debugElement.nativeElement;
    // expect(element.innerHTML).toEqual(`    {
    //       "one": -1,
    //       "two": -1
    //     }`);
    expect(element.innerHTML).toMatchSnapshot();
  });

  it('makeRequest()', () => {
    const spyLog = jest.spyOn(console, 'log');
    http.expectNone('https://givememy.data/give/it');
    component.makeRequest();
    const request = http.expectOne('https://givememy.data/give/it');
    expect(request.request.method).toBe('POST');
    expect(request.request.body).toEqual({
      my: 'param',
    });
    request.flush({
      your: 'data',
    });
    expect(spyLog).toBeCalled();
    expect(spyLog).toBeCalledWith({
      your: 'data',
    });
    http.verify();
  });

  it('use angular timings', fakeAsync(() => {
    const spyLog = jest.spyOn(console, 'log');
    component.timing();
    tick(50);
    expect(spyLog).toBeCalledTimes(0);
    tick(50);
    expect(spyLog).toBeCalledWith('great');
  }));
});
