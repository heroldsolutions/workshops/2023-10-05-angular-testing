import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Counter } from '@2023-10-05-angular-testing/testing';

@Component({
  standalone: true,
  selector: 'test-counter',
  imports: [CommonModule],
  template: `{{ output | json }}`,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CounterComponent implements OnInit {
  public variable = 0;
  public secVar: number | null = null;

  public counter1: Counter | null = null;
  public counter2: Counter | null = null;

  public output: {
    one: number;
    two: number;
  } = {
    one: -1,
    two: -1,
  };

  constructor(private readonly http: HttpClient) {}

  public ngOnInit(): void {
    this.counter1 = new Counter();
    this.counter2 = new Counter();
  }

  public generateOutput(): void {
    if (this.counter1 != null && this.counter2 != null) {
      this.output.one = this.counter1.count;
      this.output.two = this.counter2.count;
    }
  }

  public makeRequest() {
    this.http
      .post('https://givememy.data/give/it', {
        my: 'param',
      })
      .subscribe((data) => {
        console.log(data);
      });
  }

  public timing() {
    setTimeout(() => {
      console.log('great');
    }, 100);
  }
}
