const nxPreset = require('@nx/jest/preset').default;

module.exports = {
  ...nxPreset,
  coverageReporters: ['clover', 'json', 'lcov', 'text-summary'],
  collectCoverage: true,
};
