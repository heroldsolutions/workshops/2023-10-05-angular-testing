import { count } from './count';

describe('count()', () => {
  it('should return the length of an array', () => {
    // arrange
    const data: string[] = ['1', '2', '3'];
    // act
    const length = count(data);

    // assert
    expect(length).toBe(3);
  });

  it('should return -1 in case of empty array', () => {
    expect(count([])).toBe(-1);
  });
});
