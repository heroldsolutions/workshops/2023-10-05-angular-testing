export class Counter {
  private _count = 0;

  get count() {
    return this._count;
  }

  public up(): void {
    this._count++;
  }

  public down(): void {
    this._count--;
  }

  public log(): void {
    console.log(this._count);
  }
}
