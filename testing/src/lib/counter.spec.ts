import { Counter } from './counter';

describe('Counter', () => {
  let counter: Counter = new Counter();

  beforeAll(() => {
    // running before any test is started
  });
  beforeEach(() => {
    // running before every test
    counter = new Counter();
  });
  afterEach(() => {
    // after one test is finished
  });
  afterAll(() => {
    // after every test is finished
  });

  describe('up()', () => {
    it('should count up', () => {
      counter.up();

      expect(counter['_count']).toBe(1);
    });

    it('should count up twice', () => {
      counter.up();
      counter.up();

      expect(counter.count).toBe(2);
    });
  });

  it('down()', () => {
    counter.down();

    expect(counter['_count']).toBe(-1);
  });

  it('log()', () => {
    const spyLog = jest.spyOn(console, 'log');

    counter.log();

    expect(spyLog).toBeCalled();
    expect(spyLog).toBeCalledTimes(1);
    expect(spyLog).toBeCalledWith(0);
  });
});
